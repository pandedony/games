const express = require('express');
const app = require('./routes/app.js');

app(express()).listen(8000, () => {
    console.log('App is started and running at http://localhost:8000');
});