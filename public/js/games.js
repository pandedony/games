var pBatu = document.getElementById('playerBatu');
var pKertas = document.getElementById('playerKertas');
var pGunting = document.getElementById('playerGunting');

const dBatu = document.querySelector('#disableBatu');
const dKertas = document.querySelector('#disableKertas');
const dGunting = document.querySelector('#disableGunting');

var cBatu = document.getElementById('comBatu');
var cKertas = document.getElementById('comKertas');
var cGunting = document.getElementById('comGunting');

function pilihAksi(player){

  var hasil = "";
  var comMath = Math.floor(Math.random() * 3) + 1;
  var com = "";

	dBatu.setAttribute("class", "btn disabled");
	dKertas.setAttribute("class", "btn disabled");
	dGunting.setAttribute("class", "btn disabled");

	if(player == 'batu'){
		pBatu.style.backgroundColor = "grey";
	}
	else if (player == 'kertas'){
		pKertas.style.backgroundColor = "grey";
	}
	else if (player == 'gunting'){
		pGunting.style.backgroundColor = "grey";
	}
  
  switch(comMath){
      
    case 1:
			cBatu.style.backgroundColor = "grey";
      com = 'Batu';
      if (player == 'kertas'){
        hasil = "You Win";
      }else if (player == 'gunting'){
        hasil = "You Lose";
      }else{
        hasil = "Draw";
      }
      break;
      
    case 2:
			cKertas.style.backgroundColor = "grey";
      com = 'Kertas';
      if (player == 'batu'){
        hasil = "You Lose";
      }else if (player == 'gunting'){
        hasil = "You Win";
      }else{
        hasil = "Draw";
      }
      break;

		case 3:
			cGunting.style.backgroundColor = "grey";
			com = 'Gunting';
			if (player == 'kertas'){
				hasil = "You Lose";
			}else if (player == 'batu'){
				hasil = "You Win";
			}else{
				hasil = "Draw";
			}
			break;
    }
  
  document.getElementById('hasil').innerHTML = hasil;
}

function reset() {
  dBatu.removeAttribute("class", "btn disabled");
	dKertas.removeAttribute("class", "btn disabled");
	dGunting.removeAttribute("class", "btn disabled");

	pBatu.style.backgroundColor = 'transparent';
	pKertas.style.backgroundColor = 'transparent';
	pGunting.style.backgroundColor = 'transparent';
	

	cBatu.style.backgroundColor = "transparent";
	cKertas.style.backgroundColor = "transparent";
	cGunting.style.backgroundColor = "transparent";

	document.getElementById('hasil').innerHTML = 'VS';
}
