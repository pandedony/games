const { User, History, Biodata } = require("../models");

exports.reqAdmin = (req, res, next) =>{
  const admin = req.session.user;

  if(admin){
    if(admin.id === 1){
      next();
    }
    else
    {
      req.session.destroy();
      res.redirect('/');
    }
  }
  // res.json(admin.email);
};


exports.index = async ( req, res, next) => {
  var admin = req.session.user;
  const data = await History.findAll({
    include:[
      {
        model: User
      }
    ]
  });

  res.render("admin/index", {
    admin,
    data,
    layout: 'layouts/admin-layout',
    title: 'Halaman Admin',
  });
};

exports.aUsers = async ( req, res, next) => {
  var admin = req.session.user;
  const data = await User.findAll({
    where :{
      level : 2
    },
    include:[
      {
        model: Biodata,
        required: true
      }
    ]
  });
  res.json(data);
  // console.log(data.level);
  res.render("admin/user/index", {
    admin,
    data,
    layout: 'layouts/admin-layout',
    title: 'Halaman Admin',
  });
};


