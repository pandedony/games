'use strict';
const md5 = require('md5');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Users", [
      {
        email: 'pandedony@gmail.com',
        password: md5('1234'),
        level: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        email: 'pandedonyy@gmail.com',
        password: md5('1234'),
        level: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
