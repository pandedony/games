const express = require('express');
const path = require('path');
const expressLayouts = require('express-ejs-layouts');
const session = require('express-session');

const route = require("./route");

module.exports = (app) => {
    app.use(express.static('public'));
    // app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');
    app.use(expressLayouts);
    app.use(session({
        secret: 'ini adalah kode rahasia',
        resave: false,
        saveUninitialized: true,
        cookie: { maxAge : 60*60*100 }
      }));

    app.use(express.json());
    app.use(express.urlencoded({extended:true}));
    
    
    app.use(route);
    
    return app;
}; 