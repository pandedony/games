const express = require("express");
const route = express.Router();




// Controller
const homeController = require("../src/controllers/homeController");
const authController = require("../src/controllers/authController");
const adminController = require("../src/controllers/adminController");

// Home
route.get("/", homeController.index);


// Auth
route.get("/login", authController.login);
route.post("/login", authController.auth);
route.get("/register", authController.register);

// Admin
route.get("/admin", adminController.reqAdmin, adminController.index);
route.get("/admin/users", adminController.reqAdmin, adminController.aUsers);



route.get("/test", authController.test);



// test

route.get("/test", authController.test);


module.exports = route;